function isSubsetSum(set, n, sum)  { 

    if (sum == 0) { return true; }
    if (n == 0 && sum != 0) { return false; }
    if (set[n - 1] > sum) { return isSubsetSum(set, n - 1, sum); }
    return isSubsetSum(set, n - 1, sum) || isSubsetSum(set, n - 1, sum - set[n - 1]); 
} 
function v1chNum(x) {
     var r = [1];
     var n = 0;
     var m = x/2;
      for(var i = 2; i <= m; i++ ) {
          if(x%i==0) {
               if(r.indexOf(i)==-1) {
                   r.push(i);
                   n += i;
               }
                m = x/i;
                if(r.indexOf(m)==-1) {
                    r.push(m);
                    n += m;
                }                                               
            }
            
        }
        if( n > x ) {

            r.sort(function(a, b) {return b - a;});
            if(!isSubsetSum(r,r.length,x)) {
                console.log("Taps suggested by stranger :"+"\n"+x);
                
                } else { return false; }
            } else { return false; }
}
for(var x = 1; x<1000; x++) {
                    v1chNum(x);
}